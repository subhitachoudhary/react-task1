import React from "react";
import { Link } from "react-router-dom";
import axios from "axios";
import user from "./../../Images/user.png";
import "./Login.css";

const window = require("global/window");

class LoginPageDemo extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      userName: "",
      password: "",
      message: ""
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    const { name, value } = event.target;
    this.setState({
      [name]: value
    });
  }

  handleSubmit(event) {
    event.preventDefault();
    const values = this.state;
    console.log(values);
    axios
      .post("https://api.prontoitlabs.com/api/v1/user/login", values)
      .then(response => {
        window.localStorage.setItem("token", response.data.data.token);

        window.localStorage.setItem("user", JSON.stringify(response.data.user));
        window.location.href = "/dashboard";
        this.setState({
          userName: "",
          password: ""
        });
      })
      .catch(error => {
        if (error.response) {
          if (error.response.status === 401) {
            this.setState({
              message: `Invalid userName or password !`
            });
          }
          if (error.response.status === 500) {
            window.localStorage.clear();
            alert("remove token");
            window.location = "/";
          }
        } else {
          this.setState({
            message: `Technical Problem!!`
          });
        }
      });
  }

  render() {
    const { userName, password, message } = this.state;
    return (
      <div className="logInPage">
        <h2>Login Form</h2>

        <form onSubmit={this.handleSubmit}>
          <div className="imgcontainer">
            <img src={user} alt="Avatar" className="avatar" />
          </div>

          <div className="container">
            <input
              placeholder="Enter Username"
              type="text"
              name="userName"
              value={userName}
              onChange={this.handleChange}
              required
            />

            <input
              type="password"
              placeholder="Enter Password"
              name="password"
              value={password}
              onChange={this.handleChange}
              required
            />

            <button type="submit" value="Submit">
              Login
            </button>
            <p
              style={{
                color: "red",
                textDecoration: "none",
                width: "100%"
              }}
            >
              {message}
            </p>
          </div>
        </form>
        <br />
        <span
          style={{
            color: "black",
            cursor: "pointer",
            textDecoration: "none",
            marginLeft: -10
          }}
        >
          Dont have an account? &nbsp;
          <Link
            to="/signup"
            style={{ textDecoration: "underline", color: "black" }}
          >
            Signup
          </Link>
          &nbsp;now
        </span>
      </div>
    );
  }
}

export default LoginPageDemo;
