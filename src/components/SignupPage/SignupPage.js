import React from "react";
import { Link } from "react-router-dom";
import axios from "axios";
import "./SignupPage.css";
import user from "./../../Images/user.png";

const window = require("global/window");

class SignUpPageDemo extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      userName: "",
      password: "",
      gender: "MALE",
      message: ""
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    const { name } = event.target;
    const value =
      event.target.type === "checkbox"
        ? event.target.checked
        : event.target.value;
    this.setState({
      [name]: value
    });
  }

  handleSubmit(event) {
    event.preventDefault();
    const values = this.state;
    console.log(values);
    axios
      .post("https://api.prontoitlabs.com/api/v1/user", values)
      .then(response => {
        window.localStorage.setItem("key", response.data.data.token);
        this.setState({
          message: `Your Account Created Successfully.`,
          userName: "",
          password: ""
        });
        setTimeout(() => {
          window.location.href = `/`;
        }, 5000);
      })
      .catch(error => {
        if (error.response.status === 409) {
          this.setState({
            message: `User already exist. Try login!`,
            userName: "",
            password: ""
          });
          setTimeout(() => {
            window.location.href = `/`;
          }, 5000);
        }
      });
  }

  render() {
    const { password, userName, message } = this.state;
    return (
      <div className="signUpPage">
        <h2>Registration</h2>
        <form onSubmit={this.handleSubmit}>
          <div className="imgcontainer">
            <img src={user} alt="Avatar" className="avatar" />
          </div>

          <div className="container">
            <input
              placeholder="Enter Username"
              type="text"
              name="userName"
              value={userName}
              onChange={this.handleChange}
              required
            />
            <br />
            <input
              type="password"
              placeholder="Enter Password"
              name="password"
              value={password}
              onChange={this.handleChange}
              required
            />
            <label htmlFor="gender">
              <b>Gender</b>
            </label>
            <input
              type="radio"
              name="gender"
              onChange={this.handleChange}
              value="MALE"
              checked
            />
            Male
            <input
              type="radio"
              name="gender"
              onChange={this.handleChange}
              value="FEMALE"
            />
            Female
            <br />
            <button type="submit" value="Submit">
              Register
            </button>
          </div>
          <br />
          <p
            style={{
              color: "green",
              textDecoration: "none",
              width: "100%"
            }}
          >
            {message}
          </p>
        </form>

        <br />
        <div id="content" />
        <span
          style={{
            color: "black",
            cursor: "pointer",
            textDecoration: "none"
          }}
        >
          Already a Member? &nbsp;
          <Link to="/" style={{ textDecoration: "underline", color: "black" }}>
            Signin
          </Link>
          &nbsp; now
        </span>
      </div>
    );
  }
}

export default SignUpPageDemo;
