import React, { Component } from "react";
import axios from "axios";
import Fa from "react-fontawesome";
import Pagination from "./Pagination";
import "./Dashboard.css";

class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      users: [],
      currentUser: [],
      currentPage: null,
      totalElements:null,
      totalPages:null
    };
  }

  onLogout = () => {
    window.localStorage.removeItem("token");
    window.location.href = "/";
  };
  componentWillMount() {
    setInterval(() => {
      this.verifyToken();
    }, 10000);

    this.getData(0,1100);
  }
  verifyToken() {
    axios
      .post(
        `http://api.prontoitlabs.com/api/v1/user/verify-token`,
        {},
        {
          headers: {
            "X-AUTH-TOKEN": localStorage.getItem("token")
          }
        }
      )
      .then(response => {
      })
      .catch(err => {
        if (err) {
          alert("Error:" + err.errorMessage);
        } else alert("Error: session expire");
        window.location.href = "/";
      });
  }

  getData(curPage, size) {
    axios
      .get(`http://api.prontoitlabs.com/api/v1/user?page=${curPage}&size=1100`, {
        headers: {
          "X-AUTH-TOKEN": localStorage.getItem("token")
        }
      })
      .then(async response => {
        const listStatics = response.data.data;
        const users = listStatics.content;
        await this.setState({ users: users,
        totalElements : listStatics.totalElements });
      })
      .catch(error => {
        window.location.href = "/";
      });
  }
  onPageChanged = data => {
    const { users } = this.state;
    const { currentPage, totalPages, pageLimit } = data;
    const offset = (currentPage - 1) * pageLimit;
    const currentUser = users.slice(offset, offset + pageLimit);
    this.setState({ currentPage, currentUser, totalPages });
    };
  render() {
    const { users, currentUser, currentPage, totalPages, totalElements } = this.state;
    const totalUsers = users.length;

    if (totalUsers === 0) return null;

    return (
      <div className="Dashboard">
        <div className="logout">
          <h2>Dashboard</h2>
          <a
            onClick={this.onLogout}
            style={{ cursor: "grabbing", marginTop: 24 }}
            title="Logout"
            target="_blank"
          >
            <Fa name="power-off" className="powerOff" />
          </a>
        </div>
        <div className="userList">
          <div className="">
            <div className="">
              <div className="">
                <h2 className="">
                  <strong className="text-secondary">
                    Total User: {totalUsers}
                  </strong>
                </h2>
                {currentPage && (
                  <span className="current-page">
                    Page <span className="font-weight-bold">{currentPage}</span>
                    /<span className="font-weight-bold">{totalPages}</span>
                  </span>
                )}
              </div>
              <div className="align-items-center">
                <Pagination
                  totalRecords={totalElements}
                  pageLimit={25}
                  pageNeighbours={1}
                  onPageChanged={this.onPageChanged}
                />
              </div>
            </div>
            <table style={{'textAlign': 'left', 'margin': 'auto'}}>
              <tr>
                <td>Sr. No.</td>
                <th>User Name</th>
                <th>Password</th>
                <th>Gender</th>
                </tr>

            {currentUser.map((user,index) => (
              <tr>
                <td>{user.id}</td>
                <td>{user.userName}</td>
                <td>{user.password}</td>
                <td>{user.gender}</td>
                </tr>
            ))}
            </table>
          </div>
        </div>
      </div>
    );
  }
}

export default Dashboard;
