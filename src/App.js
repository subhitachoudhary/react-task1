import React, { Component } from 'react';
import { Route, Switch } from "react-router-dom";
import LoginPageDemo from "./components/LoginPage/Login";
import SignUpPageDemo from "./components/SignupPage/SignupPage";
import Dashboard from "./components/Dashboard/Dashboard";
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Switch>
            <Route exact path="/" component={LoginPageDemo} />
            <Route path="/signup" component={SignUpPageDemo} />
            <Route path="/dashboard" component={Dashboard} />
        </Switch>
      </div>
    );
  }
}

export default App;
